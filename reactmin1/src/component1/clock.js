import React from 'react';


class Clock extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            date: new Date(),
            useUTC: true
        }
        this.changFormat=this.changFormat.bind(this);    
        // แบบเก่า
    }
    componentDidMount() {
        //รันอะไรก้ได้  
        //แสดงผลเสร็จแล้วให่ทำอะไร

        this.timer = setInterval(() => this.tick(), 1000)
        //ใช้การโหลดแล้วใหม้มีการอัพเดทตัวใหม่
    }

    tick() {
        this.setState(
            {
                date: new Date()
            }
        )
    }
    changFormat(){   
            // =()=>{
        this.setState({
            useUTC :   !this.state.useUTC
        })
        this.props.onFormateChanged
    }
    render() {
        return (
        <div>
            {
                    this.state.useUTC ?
               <h2> {this.state.date.toUTCString()} </h2>
                :
               <h2>{ this.state.date.toTimeString()}</h2>
              }
                <button onClick={this.changFormat}>ChangFormat</button>
               </div>
            // <h2>   {this.state.useUTC ?
            //     this.state.date.toUTCString()
            //     :
            //     this.state.date.toTimeString()}</h2>
        )
    }
    componentWillUnmount() {
        clearInterval(this.timer);
    }
}
export default Clock